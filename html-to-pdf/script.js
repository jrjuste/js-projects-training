function printPdf() {
    //new window object can only be created with window.open()
    //in an event listener.
    //if we call it elsewhere, null will be returned
    const newWindow = window.open();

    //creating a new html mode
    const html = document.createElement("html");

    //We can load the CSS by cloning the document head
    //Note: since we are going to move node to a foreign
    //window object, the node in this original window
    //will disappear, because we have moved it to a new location
    //cloneNode(true) will performa  deep clone
    const head = document.head.cloneNode(true);

    //creating a new body element for our newWindow
    const body = document.createElement("body");

    //grab the elements that you want to convert to PDF
    const section = document.getElementById("section").cloneNode(true);

    //you can append as many child as you like
    //this is where we add our elements to the new window
    body.appendChild(section);

    html.appendChild(head);
    html.appendChild(body);

    //write content to the new window's document
    newWindow.document.write(html.innerHTML);

    //close document to stop writing
    //otherwise  new window may hang
    newWindow.document.close();

    //print content in new window as PDF
    newWindow.print();
}