
var box = document.querySelector(".box");
console.log("testing")

//detect all clicks on the document
document.addEventListener("click", function(event) {
    //if user clicks inside the element, do nothing
    if(event.target.closest(".box")) return;

    //if user clicks outside the element, hide it!
    box.classList.add("js-is-hidden");

  
});