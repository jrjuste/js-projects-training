"use strict";

var input = document.getElementById('input'), //input/output
    number = document.querySelectorAll('.numbers div'), // number buttons
    operator = document.querySelectorAll('.operators div'), //operator buttons
    result = document.getElementById('result'), //equal button
    clear = document.getElementById('clear'), //clear button
    resultDisplayed = false; //flag to keep an eye on what output is displayed

//adding click handlers to number buttons
for (var i = 0; i < number.length; i++) {
    number[i].addEventListener("click", function (e) {

        //storing current input string and its last character in variables - used later
        var currentString = input.innerHTML;
        var lastChar = currentString[currentString.length - 1];

        //if result is not displayed, just keep adding
        if (resultDisplayed === false) {
            input.innerHTML += e.target.innerHTML;
        } else if (resultDisplayed === true && lastChar === "+" || lastChar === "-" ||
            lastChar === "x" || lastChar === "+") {
            //if result is currently displayed and user pressed an operator
            //we need to keep on adding to the string for next operation
            resultDisplayed = false;
            input.innerHTML += e.target.innerHTML;
        } else {
            //if result is currently displayed and user pressed a number
            //we need to clear the input string and add the new input to start the new op
            resultDisplayed = false;
            input.innerHTML = "";
            input.innerHTML += e.target.innerHTML;
        }
    });
}

//adding click handlers to number buttons
for (var i = 0; i < operator.length; i++) {
    operator[i].addEventListener("click", function (e) {

        //storing current input string and its last character in variables - used later
        var currentString = input.innerHTML;
        var lastChar = currentString[currentString.length - 1];

        //if last character entered is an operator, replace it with the currently pressed one

        if (lastChar === "+" || lastChar === "-" || lastChar === "x" || lastChar === "÷") {
            var newString = currentString.substring(0, currentString.length - 1) + e.target.innerHTML;
            input.innerHTML = newString;
        } else if (currentString.length == 0) {
            //if first key pressed is an operator dont do anything
            console.log("enter a number first");
        } else {
            //else just add the operator pressed to the input
            input.innerHTML += e.target.innerHTML;
        }
    });
}

//onclick of 'equal button
result.addEventListener("click", function() {

     // this is the string that we will be processing eg. -10+26+33-56*34/23
     var inputString = input.innerHTML;

     // forming an array of numbers. eg for above string it will be: numbers = ["10", "26", "33", "56", "34", "23"]
     var numbers = inputString.split(/\+|\-|\×|\÷/g);

     // forming an array of operators. for above string it will be: operators = ["+", "+", "-", "*", "/"]
  // first we replace all the numbers and dot with empty string and then split

  var operators = inputString.replace(/[0-9]|\./g, "").split("");

  console.log(inputString);
  console.log(operators);
  console.log(numbers);
  console.log("-----------------------");

   // now we are looping through the array and doing one operation at a time.
  // first divide, then multiply, then subtraction and then addition
  // as we move we are alterning the original numbers and operators array
  // the final element remaining in the array will be the output

var divide = operators.indexOf("÷");
while (divide != -1) {
    numbers.splice(divide, 2, numbers[divide] / numbers[divide + 1]);
    operators.splice(divide, 1);
    divide = operators.indexOf("÷");
}


})

