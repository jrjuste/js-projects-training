//variables to display quotes and activate buttons in html

const quoteContainer = document.getElementById('quote-container');
const quoteText = document.getElementById('quote');
const authorText = document.getElementById('author');
const twitterBtn = document.getElementById('twitter');
const newQuoteBtn = document.getElementById('new-quote')
const loader = document.getElementById('loader')

//this array will hold the quotes and author info
let apiQuotes = [];

// show loading spinner
function showLoadingSpinner () {
    loader.hidden = false;
    quoteContainer.hidden = true;
}

// remove loading spinner () {
    function removeLoadingSpinner () {
        quoteContainer.hidden = false;
        loader.hidden = true;
    }

// show new quote

function newQuote () {

    //call this function to load spinner
    showLoadingSpinner();

    //pick a random quote from array
    const quote = apiQuotes[Math.floor(Math.random() * apiQuotes.length)];

    //if author field is blank, replace it with 'Unknown'
    if(!quote.author) {
        authorText.textContent = 'Unknown';
    } else {
        authorText.textContent = quote.author;
    }

    //check quote length to determine styling (long quote or short quote)
    if (quote.text.length > 115 ) {
        quoteText.classList.add('long-quote');
    } else {
        quoteText.classList.remove('long-quote')
    }

    //set Quote, hide Loader
    quoteText.textContent = quote.text;
    removeLoadingSpinner();
}

//get quotes from API

async function getQuotes () {
    showLoadingSpinner();
    const apiUrl = 'https://type.fit/api/quotes';
    try {
        const response = await fetch(apiUrl);
        apiQuotes = await response.json();
        newQuote ();
    } catch (error) {
        newQuote();
    }
}

//Tweet the quote
function tweetQuote () {
    const twitterUrl = `https://twitter.com/intent/tweet?text=${quoteText.innerText} - ${authorText.innerText}`;
  window.open(twitterUrl, '_blank');
}

//Event Listeners
newQuoteBtn.addEventListener('click', newQuote);
twitterBtn.addEventListener('click', tweetQuote);

//On load
getQuotes();
