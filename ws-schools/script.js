
//returns date
function myDate() {
    document.getElementById('displayArea').innerHTML = Date();
}

//turns light on
function lightOn() {
    document.getElementById('myImage').src = "images/pic_bulbon.gif"
}

//turns light off
function lightOff() {
    document.getElementById('myImage').src = "images/pic_bulboff.gif"
}

//change font size
function changeFont() {
    document.getElementById('changeMe').style.fontSize='25px';
}

//hide element
function hideElement() {
    document.getElementById('hideMe').style.display='none';
}

//add numbers
function addMe() {
    var a, b, c;
    a = 2;
    b = 6;
    c = b + a;
    document.getElementById('answer').innerHTML = 'The value is ' + c + '.';
}

//array
function myArray() {
    const cars = ["Subaru", "Lexus", "Toyota"];
    document.getElementById('displayArray').innerHTML = cars[1];
}

//show objects
function myObject() {
    const person = {
        lastName : "Smith",
        firstName: "Joe",
        age: 50
    }
    document.getElementById('displayObject').innerHTML = 'My name is ' + person.firstName + ' ' + person.lastName + '. I am '
    + person.age + ' years old.'
}

//typeof Operator
function dataTypes() {
    document.getElementById('displayTypeof').innerHTML = typeof 'Sunflower' + '<br>' + typeof 6 + '<br>'  + typeof 0 + '<br>'+ typeof -15.4;
}

//functions with arguments


  