const toggleSwitch = document.querySelector('input[type="checkbox"]');
const nav = document.getElementById('nav');
const toggleIcon = document.getElementById('toggle-icon');
const image1 = document.getElementById('image1');
const image2 = document.getElementById('image2');
const image3 = document.getElementById('image3');

//dark or light images
function imageMode(color) {
    image1.src = `img/undraw_finance_${color}.svg`;
    image2.src = `img/undraw_make_it_rain_${color}.svg`;
    image3.src = `img/undraw_winners_${color}.svg`;
}

function darkMode () {
    nav.style.backgroundColor = 'black';
    toggleIcon.children[0].textContent = 'Dark Mode';
    toggleIcon.children[1].classList.replace('fa-sun', 'fa-moon');
    imageMode('dark');
    document.body.style.backgroundColor = "#121212"
    document.body.style.color = "white"
}