# js-projects-training
Repo containing js projects used for training

# number-guessing-name
https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/A_first_splash

# intro to javascript
https://www.guru99.com/introduction-to-javascript.html

# js-tip-calculator
https://skillcrush.com/blog/projects-you-can-do-with-javascript/
https://codepen.io/cphemm/pen/reNwWd

# to do list
https://skillcrush.com/blog/projects-you-can-do-with-javascript/
https://codepen.io/JohnPaulFich/pen/MXmzzM

# quote generator
https://betterprogramming.pub/i-built-20-web-projects-with-vanilla-javascript-in-20-days-4a1cd91a3760
Link: https://5ebs.github.io/quote-generator/
GitHub: https://github.com/5ebs/quote-generator

# light-dark mode
https://betterprogramming.pub/i-built-20-web-projects-with-vanilla-javascript-in-20-days-4a1cd91a3760
Link: https://5ebs.github.io/light-dark-mode/
GitHub: https://github.com/5ebs/light-dark-mode

# vanilla js stopwatch
https://mikkegoes.com/javascript-projects-for-beginners/
https://codepen.io/cathydutton/pen/xxpOOw

# calculator
https://mikkegoes.com/javascript-projects-for-beginners/
https://codepen.io/lalwanivikas/details/eZxjqo

# animated-nav-menu
https://mikkegoes.com/javascript-projects-for-beginners/
https://codepen.io/AJamesL/pen/MXmvZp

# animated nav
https://github.com/5ebs/animated-navigation
https://5ebs.github.io/animated-navigation/#contact

# vanilla js box
https://codepen.io/tylerama/pen/nEGpZp

# vanilla js practice
https://codepen.io/mike-shine/pen/abpJNxN

# html to pdf
https://gist.github.com/sam-ngu/ee10b650112f891013271b8d7ca3e6f3

# Hide Box
https://codepen.io/StrengthandFreedom/pen/LeyjXG

# ajax demo
https://codepen.io/tutsplus/pen/vLpGKp

# js scrolling
https://codepen.io/nikhilroy2/pen/PoNLbZY

# w3 schools js projects
https://www.w3schools.com/js/
