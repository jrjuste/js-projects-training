var enterButton = document.getElementById("enter");
var input = document.getElementById("userInput");
var ul = document.querySelector("ul");
var item = document.getElementsByTagName("li");

function inputLength() {
    return input.value.length;
}

function listLength() {
    return item.length;
}

function createListElement() {
    var li = document.createElement("li") //creates list

    li.appendChild(document.createTextNode(input.value));

    //makes text from input field the li text
    ul.appendChild(li); //adds li to ul
    input.value = ""; //reset text input field

    //start strikethrough
    //because its in the function, it ony addds it for new items

    function crossOut() {
        li.classList.toggle("done");
    }

    li.addEventListener("click", crossOut);
    //end strikethrough

    //Start and delete button
    var d8tn = docuent.createElement("button");
    d8tn.appendChild(document.createTextNode("X"));
    li.appendChild(d8tn);
    d8tn.addEventListener("click", deleteListItem);
    //end add delete button

    //add class delete (display: none)
    function deleteListItem() {
        li.classList.add("delete")
    }
    //end add class delete
}

function addListAfterClick (){
    if(inputLength () > 0) {
        //makes sure an empty input field doesnt create an ali
        createListElement();
    }
}

function addListAfterKeypress(event) {
    if(inputLength() > 0 && event.which ===13) {
        //this now looks to see if you hit enter
        //the 13 is the enter key's code
        createListElement();
    }
}

enterButton.addEventListener("click", addListAfterClick);
input.addEventListener("keypress", addListAfterKeypress);