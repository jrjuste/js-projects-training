//first we detect the click event
document.getElementById('the-box').addEventListener('click', function() {
    //using an if statement to check the class
    if (this.classList.contains('bad')) {
        //the box that we clicked has a class of bad so let's remove it and add the good class
        this.classList.remove('bad');
        this.classList.add('good');
    } else {
        //we tell the user what happens next
        alert("you can proceed");
    }
});