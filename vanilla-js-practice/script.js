

//declare variable for header
const header = document.createElement('h1');

//populate header's text content with string
header.textContent = 'Vanilla JS Practice';

//append this new element to body
document.body.appendChild(header);

//declare variable in paragraph tag
const textOne = document.createElement('p');

//populate text content with string
textOne.textContent = 'I am text one';

//append new element to the body
document.body.appendChild(textOne);

//declare variable in paragraph tag
const textTwo = document.createElement('p');

//poplualte text content with string in new variable
textTwo.textContent = " And I am text two";

document.body.appendChild(textTwo);