function moveIt(){

    var innerDiv = document.getElementsByClassName("insideDiv")[0];
    var outerDiv = innerDiv.parentElement;

    if(outerDiv.nextElementSibling != null) {
        outerDiv.nextElementSibling.appendChild(outerDiv.removeChild(innerDiv));
    } else {
        //wrap-around if else statement, this needs a scope to operate in
        //scope is set by <div class="container">..</container>
        outerDiv.parentElement.firstElementChild.appendChild(
            outerDiv.removeChild(innerDiv)
        )
    }
}